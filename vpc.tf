resource "aws_vpc" "services" {
  cidr_block           = local.svc_cidr_block[local.environment]
  enable_dns_hostnames = true
  tags = merge(
    local.common_tags,
    { Name = local.svc_vpc_name }
  )
}

resource "aws_default_security_group" "default" {
  vpc_id = aws_vpc.services.id
  tags = merge(
    local.common_tags,
    { Name = "${local.svc_vpc_name}-default" }
  )
}

resource "aws_flow_log" "flow_log" {
  iam_role_arn    = aws_iam_role.svc_vpc_flow_logs.arn
  log_destination = aws_cloudwatch_log_group.svc_vpc_flow_logs.arn
  traffic_type    = "ALL"
  vpc_id          = aws_vpc.services.id
  tags = merge(
    local.common_tags,
    { Name = local.svc_vpc_name }
  )
}

resource "aws_cloudwatch_log_group" "svc_vpc_flow_logs" {
  name              = "/aws/vpc-flow-logs/${local.svc_vpc_name}"
  retention_in_days = 30

  tags = merge(
    local.common_tags,
    { Name = local.svc_vpc_name }
  )
}

data "aws_iam_policy_document" "vpc_flow_logs_assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["vpc-flow-logs.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

data "aws_iam_policy_document" "svc_vpc_flow_logs" {
  statement {
    effect    = "Allow"
    resources = [format("%s:*", aws_cloudwatch_log_group.svc_vpc_flow_logs.arn)]

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents",
      "logs:DescribeLogGroups",
      "logs:DescribeLogStreams",
    ]
  }
}

resource "aws_iam_role" "svc_vpc_flow_logs" {
  name               = "vpc_flow_logs_${local.svc_vpc_name}"
  assume_role_policy = data.aws_iam_policy_document.vpc_flow_logs_assume_role.json

  tags = merge(
    local.common_tags,
    { Name = local.svc_vpc_name }
  )
}

resource "aws_iam_policy" "svc_vpc_flow_logs" {
  name        = "vpc_flow_logs_${local.svc_vpc_name}"
  policy      = data.aws_iam_policy_document.svc_vpc_flow_logs.json
  description = "Allows writing of VPC flow logs for the ${local.svc_vpc_name} VPC"
}

resource "aws_iam_role_policy_attachment" "svc_vpc_flow_logs" {
  role       = aws_iam_role.svc_vpc_flow_logs.name
  policy_arn = aws_iam_policy.svc_vpc_flow_logs.arn
}

resource "aws_subnet" "services" {
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.services.id
  availability_zone = data.aws_availability_zones.available.names[count.index]
  cidr_block        = cidrsubnet(aws_vpc.services.cidr_block, 2, count.index)
  tags = merge(
    local.common_tags,
    { Name = "${local.svc_vpc_name}-${data.aws_availability_zones.available.names[count.index]}" }
  )
}

resource "aws_route_table" "services" {
  vpc_id = aws_vpc.services.id
  tags = merge(
    local.common_tags,
    { Name = "services-${local.environment}" }
  )
}

resource "aws_route_table_association" "services" {
  count          = length(aws_subnet.services.*.id)
  subnet_id      = aws_subnet.services[count.index].id
  route_table_id = aws_route_table.services.id
}

resource "aws_security_group" "vpc_endpoints" {
  vpc_id      = aws_vpc.services.id
  name        = "vpc-endpoints-${local.environment}"
  description = "Rules for accessing VPC endpoints in ${local.environment}"
  tags = merge(
    local.common_tags,
    { Name = "vpc-endpoints-${local.environment}" }
  )
}

resource "aws_vpc_endpoint" "logs" {
  vpc_id              = aws_vpc.services.id
  service_name        = "com.amazonaws.${data.aws_region.current.name}.logs"
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.vpc_endpoints.id]
  private_dns_enabled = true
  tags = merge(
    local.common_tags,
    { Name = "logs-${local.environment}" }
  )
}

resource "aws_vpc_endpoint_subnet_association" "logs" {
  count           = length(aws_subnet.services.*.id)
  vpc_endpoint_id = aws_vpc_endpoint.logs.id
  subnet_id       = aws_subnet.services[count.index].id
}

resource "aws_vpc_endpoint" "ecr_dkr" {
  vpc_id              = aws_vpc.services.id
  service_name        = "com.amazonaws.${data.aws_region.current.name}.ecr.dkr"
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.vpc_endpoints.id]
  private_dns_enabled = true
  tags = merge(
    local.common_tags,
    { Name = "ecr.dkr-${local.environment}" }
  )
}

resource "aws_vpc_endpoint_subnet_association" "ecr_dkr" {
  count           = length(aws_subnet.services.*.id)
  vpc_endpoint_id = aws_vpc_endpoint.ecr_dkr.id
  subnet_id       = aws_subnet.services[count.index].id
}

resource "aws_vpc_endpoint" "ecr_api" {
  vpc_id              = aws_vpc.services.id
  service_name        = "com.amazonaws.${data.aws_region.current.name}.ecr.api"
  vpc_endpoint_type   = "Interface"
  security_group_ids  = [aws_security_group.vpc_endpoints.id]
  private_dns_enabled = true
  tags = merge(
    local.common_tags,
    { Name = "ecr.api-${local.environment}" }
  )
}

resource "aws_vpc_endpoint_subnet_association" "ecr_api" {
  count           = length(aws_subnet.services.*.id)
  vpc_endpoint_id = aws_vpc_endpoint.ecr_api.id
  subnet_id       = aws_subnet.services[count.index].id
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id            = aws_vpc.services.id
  service_name      = "com.amazonaws.${data.aws_region.current.name}.s3"
  vpc_endpoint_type = "Gateway"
  route_table_ids   = [aws_route_table.services.id]
  tags = merge(
    local.common_tags,
    { Name = "s3-${local.environment}" }
  )
}

# nsjsa-new-infra

Deploys some POC infrastructure

## How to deploy

This repo currently requires you to have an AWS CLI profile configured in `~/.aws/config` and `~/.aws/credentials` with an IAM user that has sufficient privileges to deploy resources.

```sh
export AWS_PROFILE=your_profile
export AWS_REGION=your_region
export AWS_ACCOUNT=012345678901
terraform init
# create the ECR repo first.
terraform apply -target aws_ecr_repository.bank_details
# create a container image and push to the repo
./build-and-push-container.sh
# with the deployable image now in place, the full TF deployment can proceed
terraform apply
```

## TODO

CI pipeline considerations:

1. If repos containing container-image pipelines can push to GitLab CI repositories, it will avoid the need for a bootstrapping phase as they'll be able to build and push container images without any AWS infra in place
1. In that scenario, CI should:
     * Dev:
       * Run a targetted apply to create all required ECR repositories
       * Pull all required container images from GitLab registry, push to ECR repositories
       * Run `terraform apply`
       * Run end to end tets
     * Downstream non-prod environments:
       * Guarded by previous enviroment's successful end to end tests
       * Run a targetted apply to create all required ECR repositories
       * Pull all container images deployed in dev, retag and push to env's ECR repository
       * Run `terraform apply`
       * Run end to end tests
     * Downstream prod environment
       * Guarded by previous environment's successful end to end tests
       * Run a targetted apply to create all required ECR repositories
       * Use ECR mirroring to pull, retag and push to env's ECR repository
       * Run `terraform apply`
       * Run end to end tests

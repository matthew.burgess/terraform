[
    {
        "name": "${container_name}",
        "image": "${ecr_repository}:${image_tag}",
        "essential": true,
        "portMappings": [
            {
                "containerPort": ${container_port},
                "hostPort": ${container_port}
            }
        ],
        "logConfiguration": {
            "logDriver": "awslogs",
            "options": {
                "awslogs-group": "${log_group}",
                "awslogs-region": "${log_region}",
                "awslogs-stream-prefix": "${log_stream_prefix}"
            }
        }
    }
]

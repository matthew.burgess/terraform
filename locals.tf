locals {
  common_tags = {
    Name        = ""
    Costcode    = ""
    Owner       = "nsjsa"
    Environment = local.environment
    Persistence = "Ignore"
  }

  svc_cidr_block = {
    development = "10.0.0.0/23"
    testing     = "10.1.0.0/23"
  }

  svc_vpc_name = "services-${local.environment}"
  environment  = terraform.workspace == "default" ? "development" : terraform.workspace

}

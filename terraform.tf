terraform {
  backend "s3" {
    bucket = "matts-tfstate"
    key    = "terraform/state/nsjsa-infra.tfstate"
    region = "eu-west-2"
  }

  required_version = "0.14.3"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.23.0"
    }
  }
}

provider "aws" {
  region = var.region
}

data "aws_availability_zones" "available" {}
data "aws_region" "current" {}
data "aws_caller_identity" "current" {}

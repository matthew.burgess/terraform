locals {
  bank_details_svc_name = "bank-details"
  bank_details_env_name = "${local.bank_details_svc_name}-${local.environment}"
  bank_details_port     = 8080
}

resource "aws_ecr_repository" "bank_details" {
  name                 = local.bank_details_env_name
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
  tags = merge(
    local.common_tags,
    { Name = local.bank_details_env_name }
  )
}

resource "aws_ecs_service" "bank_details" {
  name             = local.bank_details_env_name
  cluster          = aws_ecs_cluster.nsjsa.id
  launch_type      = "FARGATE"
  platform_version = "1.4.0"
  task_definition  = aws_ecs_task_definition.bank_details.arn
  desired_count    = 3
  load_balancer {
    target_group_arn = aws_lb_target_group.bank_details.arn
    container_name   = local.bank_details_svc_name
    container_port   = local.bank_details_port
  }
  network_configuration {
    subnets         = aws_subnet.services.*.id
    security_groups = [aws_security_group.bank_details.id]
  }
  tags = merge(
    local.common_tags,
    { Name = local.bank_details_env_name }
  )
  depends_on = [aws_lb.services]
}

resource "aws_security_group" "bank_details" {
  vpc_id      = aws_vpc.services.id
  name        = local.bank_details_env_name
  description = "SG for the Bank Details service in ${local.environment}"
  tags = merge(
    local.common_tags,
    { Name = local.bank_details_env_name }
  )
}

resource "aws_security_group_rule" "bank_details_to_vpc_endpoints" {
  description              = "Allow bank-details to reach VPC endpoints"
  security_group_id        = aws_security_group.bank_details.id
  source_security_group_id = aws_security_group.vpc_endpoints.id
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  type                     = "egress"
}

resource "aws_security_group_rule" "bank_details_to_s3_endpoint" {
  description       = "Allow bank-details to reach S3 endpoint"
  security_group_id = aws_security_group.bank_details.id
  prefix_list_ids   = [aws_vpc_endpoint.s3.prefix_list_id]
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  type              = "egress"
}

resource "aws_security_group_rule" "vpc_endpoints_from_bank_details" {
  description              = "Allow bank-details to reach VPC endpoints"
  security_group_id        = aws_security_group.vpc_endpoints.id
  source_security_group_id = aws_security_group.bank_details.id
  from_port                = 443
  to_port                  = 443
  protocol                 = "tcp"
  type                     = "ingress"
}

resource "aws_security_group_rule" "services_lb_to_bank_details_egress" {
  description              = "Allow services LB to reach the bank-details service"
  security_group_id        = aws_security_group.services_lb.id
  source_security_group_id = aws_security_group.bank_details.id
  from_port                = local.bank_details_port
  to_port                  = local.bank_details_port
  protocol                 = "tcp"
  type                     = "egress"
}

resource "aws_security_group_rule" "services_lb_to_bank_details_ingress" {
  description              = "Allow services LB to reach the bank-details service"
  security_group_id        = aws_security_group.bank_details.id
  source_security_group_id = aws_security_group.services_lb.id
  from_port                = local.bank_details_port
  to_port                  = local.bank_details_port
  protocol                 = "tcp"
  type                     = "ingress"
}

resource "aws_security_group_rule" "bank_details-to-services_lb_ingress" {
  description              = "Allow bank-details to reach other services through the LB"
  security_group_id        = aws_security_group.services_lb.id
  source_security_group_id = aws_security_group.bank_details.id
  from_port                = local.services_lb_port
  to_port                  = local.services_lb_port
  protocol                 = "tcp"
  type                     = "ingress"
}

resource "aws_security_group_rule" "bank_details-to-services_lb_egress" {
  description              = "Allow bank-details to reach other services through the LB"
  security_group_id        = aws_security_group.bank_details.id
  source_security_group_id = aws_security_group.services_lb.id
  from_port                = local.services_lb_port
  to_port                  = local.services_lb_port
  protocol                 = "tcp"
  type                     = "egress"
}

resource "aws_ecs_task_definition" "bank_details" {
  family = "bank-details-${local.environment}"
  container_definitions = templatefile("task-definitions/bank_details.json.tpl", {
    ecr_repository    = aws_ecr_repository.bank_details.repository_url
    container_name    = local.bank_details_svc_name
    container_port    = local.bank_details_port
    image_tag         = "latest"
    log_group         = aws_cloudwatch_log_group.bank_details.name
    log_region        = data.aws_region.current.name
    log_stream_prefix = "bank-details"
  })
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "1024"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  tags = merge(
    local.common_tags,
    { Name = local.bank_details_env_name }
  )
}

resource "aws_kms_key" "bank_details_logs" {
  description             = "Encrypts Bank Details service CloudWatch logs"
  policy                  = data.aws_iam_policy_document.bank_details_logs_key_policy.json
  enable_key_rotation     = true
  deletion_window_in_days = 7
  tags = merge(
    local.common_tags,
    { Name = "bank-details-logs-${local.environment}" }
  )
}

data "aws_iam_policy_document" "bank_details_logs_key_policy" {
  statement {
    sid    = "Enable IAM User Permissions"
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"]
    }
    actions   = ["kms:*"]
    resources = ["*"]
  }
  statement {
    sid    = "Enable CloudWatch access"
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["logs.${data.aws_region.current.name}.amazonaws.com"]
    }
    actions = [
      "kms:Encrypt*",
      "kms:Decrypt*",
      "kms:ReEncrypt*",
      "kms:GenerateDataKey*",
      "kms:Describe*",
    ]
    resources = ["*"]
    condition {
      test     = "ArnEquals"
      variable = "kms:EncryptionContext:aws:logs:arn"
      // hardcode this ARN to avoid a cyclical dependency between the KMS key and the CloudWatch Log Group
      values = ["arn:aws:logs:${data.aws_region.current.name}:${data.aws_caller_identity.current.account_id}:log-group:bank-details-${local.environment}"]
    }
  }
}

resource "aws_kms_alias" "bank_details_logs" {
  name          = "alias/bank-details-logs-${local.environment}"
  target_key_id = aws_kms_key.bank_details_logs.key_id
}

resource "aws_cloudwatch_log_group" "bank_details" {
  name              = "bank-details-${local.environment}"
  retention_in_days = 60
  kms_key_id        = aws_kms_key.bank_details_logs.arn
  tags = merge(
    local.common_tags,
    { Name = local.bank_details_env_name }
  )
}

resource "aws_lb_target_group" "bank_details" {
  name_prefix = "bd-"
  port        = local.bank_details_port
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = aws_vpc.services.id

  health_check {
    path = "/hello"
  }

  tags = merge(
    local.common_tags,
    { Name = local.bank_details_env_name }
  )
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener_rule" "bank_details" {
  listener_arn = aws_lb_listener.services.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.bank_details.arn
  }

  condition {
    path_pattern {
      values = ["/bank"]
    }
  }
}

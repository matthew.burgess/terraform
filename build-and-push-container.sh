#!/bin/sh

if [ -z "${AWS_ACCOUNT}" -o -z "${AWS_REGION}" -o -z "${AWS_PROFILE}" ]; then
    echo "Error: You must set the following environment variables before running this script"
    echo "AWS_ACCOUNT: The AWS account number holding the ECR repository"
    echo "AWS_REGION: The AWS region holding the ECR repository"
    echo "AWS_PROFILE: The AWS Profile (in ~/.aws/credentials) to use to authenticate with AWS"
    exit 1
fi;

ECR_REPO="${AWS_ACCOUNT}.dkr.ecr.${AWS_REGION}.amazonaws.com/bank-details-development"
docker build -t bank-details-development .
docker tag bank-details-development "${ECR_REPO}"
docker images --filter reference=bank-details-development
aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT.dkr.ecr.$AWS_REGION.amazonaws.com
docker push "${ECR_REPO}"

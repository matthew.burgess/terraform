resource "aws_ecs_cluster" "nsjsa" {
  // AWS will create a FARGATE cluster by default
  name = "nsjsa-${local.environment}"

  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  //although not strictly required, this ensures that Fargate doesn't try to start any tasks before all of the necessary network connectivity is in place
  depends_on = [
    aws_vpc_endpoint_subnet_association.ecr_dkr,
    aws_vpc_endpoint_subnet_association.ecr_api,
    aws_vpc_endpoint_subnet_association.logs,
    aws_vpc_endpoint.s3
  ]
}

data "aws_iam_policy_document" "ecs_assume_role" {
  statement {
    effect = "Allow"
    principals {
      type        = "Service"
      identifiers = ["ecs.amazonaws.com"]

    }
    actions = [
      "sts:AssumeRole",
    ]
  }
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name               = "ECSTaskExecutionRole-${local.environment}"
  assume_role_policy = data.aws_iam_policy_document.ecs_tasks_assume_role_policy.json
}

data "aws_iam_policy_document" "ecs_tasks_assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role_policy_attachment" "ecs_task_execution_role_policy" {
  role       = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

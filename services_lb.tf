locals {
  services_lb_port = 80
}

resource "aws_security_group" "services_lb" {
  vpc_id      = aws_vpc.services.id
  name        = "services-lb-${local.environment}"
  description = "SG for the Services ALB in ${local.environment}"
  tags = merge(
    local.common_tags,
    { Name = "services-lb-${local.environment}" }
  )
}

resource "aws_lb" "services" {
  name               = "services-${local.environment}"
  internal           = true
  load_balancer_type = "application"
  security_groups    = [aws_security_group.services_lb.id]
  subnets            = aws_subnet.services.*.id
  tags = merge(
    local.common_tags,
    { Name = "services-${local.environment}" }
  )
}

resource "aws_lb_listener" "services" {
  load_balancer_arn = aws_lb.services.arn
  port              = local.services_lb_port
  protocol          = "HTTP"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "LB path/target not found"
      status_code  = "400"
    }
  }
}
